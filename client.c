#include "include/Client.h"

int main(int argc,char * argv[]){
	G_VALUE sender;
	G_VALUE pointer;
	char buffer[BUFFER_SIZE];

	S_VALUECREATE(&sender,0,argv[3]);

	int sock_cli = client_init(atoi(argv[2]),argv[1]);
        client_message(sock_cli,&sender);
	recv(sock_cli,buffer,sizeof(buffer),0);
	memcpy(&pointer,buffer,sizeof(buffer));
	g_print("client_recv: message is: %s\ttimemos is: %d\n\n",pointer.g_message,pointer.g_timemos);
	return (0);
}
