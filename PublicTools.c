#include "include/fc.h"

gchar *FileKey_parser(gchar *scope,gchar *key){
	GKeyFile *key_file = g_key_file_new();
	GError *error = NULL;
	
	if(g_key_file_load_from_file(key_file,(const gchar*)"settings.ini",G_KEY_FILE_NONE,&error) == 0){
		g_print("error is: %s\n",error->message);
		exit(0);
	}
	return (gchar *)g_key_file_get_string(key_file,scope,key,&error);
}

void S_VALUECREATE(struct G_VALUE *pointer,gint g_timemos,gchar g_message[200]){
        pointer->g_timemos = g_timemos;
	memcpy(&pointer->g_message,g_message,sizeof(g_message)+1);
}

//struct G_VALUE* MsgHandler(struct G_VALUE *message){
//        /*gint local_g_timemos = atoi(FileKey_parser("local_machine","time_clock"));
//        message->g_timemos = message->g_timemos + local_g_timemos + 1;// number 1 is the consumptions among nodes*/
//
//        /*g_sprintf(message->g_message,"this message is from server %s and timemos is: %d",(gchar const *)FileKey_parser("local_machine","name"),message->g_timemos);
//        S_VALUECREATE(message,message->g_timemos,message->g_message);*/
//        //g_usleep(local_g_timemos*100000);
//	g_print("MsgHandler: message is: %s\ttimemos is: %d\n",message->g_message,message->g_timemos);
//        return message;
//}

void ErrorOutput(gchar *ErrorMsg){
	perror(ErrorMsg);
	//exit(0);
}
