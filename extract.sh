gcc -Wall ServerBase.c PublicTools.c server.c `pkg-config --cflags --libs glib-2.0` -o server
gcc -Wall ClientBase.c PublicTools.c client.c `pkg-config --cflags --libs glib-2.0` -o client
#gcc -Wall PublicTools.c mainapp.c `pkg-config --cflags --libs glib-2.0` -o mainapp

./server 127.0.0.1 8080 &
./client 127.0.0.1 8080 ls

sleep 3
killall server
